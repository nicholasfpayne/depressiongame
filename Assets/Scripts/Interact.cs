﻿using UnityEngine;
using System.Collections;

public class Interact : MonoBehaviour
{
	string commandPrompt;
	public AudioClip tvSound;
	public AudioClip showerSound;
	public AudioClip fridgeSound;
	public AudioClip gunshotSound;
	public AudioClip ovenSound;
	public AudioClip workSound;
	public AudioClip toiletSound;
	public AudioClip weightsSound;
	public AudioClip sleepSound;
	public AudioClip sinkSound;
	public AudioClip computerSound;
	public AudioClip phoneSound;
	private AudioSource sound;
	private CharacterMotor motion;
	public Texture blackScreen;
	private float alphaFadeValue;
	private GameObject GameController;
	private GameController gameController;

	void Start()
	{
		GameController = GameObject.FindWithTag("GameController");
		gameController = GameController.GetComponent<GameController>();
		sound = gameObject.GetComponent<AudioSource>();
		motion = gameObject.GetComponent<CharacterMotor>();
		alphaFadeValue = 1;
		InteractWith("Bed");
	}
	
	
	void CommandPrompt(string name)
	{
		if(name == "TV")
		{
			commandPrompt = "Watch TV";
		}
		else if(name == "Shower")
		{
			commandPrompt = "Take a shower";
		}
		else if(name == "Fridge")
		{
			commandPrompt = "Prepare meal";
		}
		else if(name == "Gun" && gameController.attributes["morale"] < 20f)
		{
			commandPrompt = "End the suffering";
		}
		else if(name == "Oven")
		{
			commandPrompt = "Prepare meal";
		}
		else if(name == "Door")
		{
			commandPrompt = "Go to work";
		}
		else if(name == "Toilet")
		{
			commandPrompt = "Use the restroom";
		}
		else if(name == "Dumbbells")
		{
			commandPrompt = "Lift weights";
		}
		else if(name == "Bed")
		{
			commandPrompt = "Go to bed";
		}
		else if(name == "Bathroom Sink")
		{
			commandPrompt = "Wash hands";
		}
		else if(name == "Computer")
		{
			commandPrompt = "Work from home";
		}
		else if(name == "Phone" && gameController.attributes["morale"] < 20f)
		{
			commandPrompt = "Save yourself";
		}
		else
		{
			commandPrompt = null;
		}
	}
	
	void InteractWith(string name)
	{
		if(name == "TV")
		{
			sound.clip = tvSound;
			gameController.fatigueMultiplier = 5f;
			gameController.frameTime += 1f;
			gameController.attributes["entertainment"] += 10f;
		}
		else if(name == "Shower")
		{
			sound.clip = showerSound;
			gameController.fatigueMultiplier = 10f;
			gameController.frameTime += 0.5f;
			gameController.attributes["hygiene"] += 50f;
		}
		else if(name == "Fridge")
		{
			sound.clip = fridgeSound;
			gameController.fatigueMultiplier = 10f;
			gameController.frameTime += 0.5f;
			gameController.attributes["satiety"] += 30f;
			gameController.attributes["fitness"] -= 10f;
		}
		else if(name == "Gun" && gameController.attributes["morale"] < 20f)
		{
			sound.clip = gunshotSound;
			alphaFadeValue = 1;
			GameController.SetActive(false);
		}
		else if(name == "Oven")
		{
			sound.clip = ovenSound;
			gameController.fatigueMultiplier = 10f;
			gameController.frameTime += 0.5f;
			gameController.attributes["satiety"] += 30f;
			gameController.attributes["fitness"] -= 10f;
		}
		else if(name == "Door")
		{
			sound.clip = workSound;
			gameController.fatigueMultiplier = 7.5f;
			gameController.frameTime += 8f;
			gameController.attributes["money"] += 30f;
		}
		else if(name == "Toilet")
		{
			sound.clip = toiletSound;
		}
		else if(name == "Dumbbells")
		{
			sound.clip = weightsSound;
			gameController.fatigueMultiplier = 20f;
			gameController.frameTime += 1f;
			gameController.attributes["hygiene"] -= 25f;
			gameController.attributes["fitness"] += 30f;
		}
		else if(name == "Bed")
		{
			sound.clip = sleepSound;
			if (Time.time > 3)
			{
				gameController.fatigueMultiplier = 0f;
				gameController.frameTime += 8f;
				gameController.attributes["energy"] += gameController.attributes["morale"];
			}
		}
		else if(name == "Bathroom Sink")
		{
			sound.clip = sinkSound;
			gameController.fatigueMultiplier = 5f;
			gameController.frameTime += 0.25f;
			gameController.attributes["hygiene"] += 10f;
		}
		else if(name == "Computer")
		{
			sound.clip = computerSound;
			gameController.fatigueMultiplier = 5f;
			gameController.frameTime += 3f;
			gameController.attributes["money"] += 10f;
		}
		else if(name == "Phone" && gameController.attributes["morale"] < 20f)
		{
			sound.clip = phoneSound;
			GameController.SetActive(false);
		}
		else
			return;

		sound.Play();
	}
	
	void Update ()
	{
		if(sound.isPlaying)
		{
			motion.enabled = false;
		}
		else
		{
			motion.enabled = true;
		}
		
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		CommandPrompt(null);
		if (Physics.Raycast(ray, out hit, 3))
		{
			Debug.DrawLine(ray.origin, hit.point);
			if (hit.collider.tag == "Interactable")
			{
				CommandPrompt(hit.collider.name);
				
				if ((Input.GetKeyDown(KeyCode.E) || Input.GetMouseButtonDown(0)))
				{
					InteractWith(hit.collider.name);
					Debug.Log (hit.collider.name);
				}
			}
		}
		
	}

	void OnGUI()
	{
		if(sound.isPlaying || sound.clip == gunshotSound || sound.clip == phoneSound )
		{
			alphaFadeValue = Mathf.Clamp01(alphaFadeValue += Time.deltaTime / 3);
			GUI.color = new Color(0, 0, 0, alphaFadeValue);
			GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), blackScreen );
			gameController.timeScale = 0f;
		}
		else if(alphaFadeValue > 0)
		{
			alphaFadeValue = Mathf.Clamp01(alphaFadeValue -= Time.deltaTime / 3);
			GUI.color = new Color(0, 0, 0, alphaFadeValue);
			GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), blackScreen );
			gameController.timeScale = 1f / 18.75f;
			gameController.fatigueMultiplier = 1f;
		}
		
		GUI.Label(new Rect(Screen.width/2, Screen.height/10, Screen.width/5, Screen.height/5), commandPrompt);
	}
}
