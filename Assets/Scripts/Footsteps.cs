﻿using UnityEngine;
using System.Collections;

public class Footsteps : MonoBehaviour
{
	void Update ()
	{
		AudioSource steps = gameObject.GetComponent<AudioSource>();
		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
			steps.mute = false;
		else
			steps.mute = true;
	}
}
