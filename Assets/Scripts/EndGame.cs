﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {

	public bool endGame = false;

	private GameObject player;
	private GameObject mainCamera;
	private GameObject GameController;
	private GameController gameController;

	void Awake()
	{
		GameController = GameObject.FindWithTag("GameController");
		gameController = GameController.GetComponent<GameController>();
		player = GameObject.FindWithTag("Player");
		mainCamera = GameObject.FindWithTag("MainCamera");
	}
	// Update is called once per frame
	void Update () 
	{
		if (gameController.attributes["morale"] <= 0)
		{
			player.GetComponent<CharacterController>().enabled = false;
			endGame = true;
		}
		if (endGame)
		{
			Vector3 lookDir = gameObject.transform.position - mainCamera.transform.position;
			Vector3 newDir = Vector3.RotateTowards(mainCamera.transform.forward, lookDir, Time.deltaTime * 30f, 0.0F);
			Debug.DrawRay(mainCamera.transform.position, newDir);
			mainCamera.transform.rotation = Quaternion.LookRotation(newDir);
		}
	}
}
