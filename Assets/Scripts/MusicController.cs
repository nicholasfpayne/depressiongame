﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour
{
	public AudioClip HappySong;
	public AudioClip MidHappySong;
	public AudioClip MidSong;
	public AudioClip MidSadSong;
	public AudioClip SadSong;
	private AudioSource music;
	private GameController gameController;
	// Use this for initialization
	void Start ()
	{
		gameController = gameObject.transform.parent.GetComponent<GameController>();
		music = gameObject.GetComponent<AudioSource>();
		music.clip = HappySong;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!music.isPlaying)
		{
			if(gameController.attributes["morale"] >= 80)
			{
				music.clip = HappySong;
				music.Play();
			}
			else if(gameController.attributes["morale"] >= 60)
			{
				music.clip = MidHappySong;
				music.Play();
			}
			else if(gameController.attributes["morale"] >= 40)
			{
				music.clip = MidSong;
				music.Play();
			}
			else if(gameController.attributes["morale"] >= 20)
			{
				music.clip = MidSadSong;
				music.Play();
			}
			else 
			{
				music.clip = SadSong;
				music.Play();
			}
		}
	}
}
