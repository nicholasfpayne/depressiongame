﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public int dayCount, clockHour, clockMinute;
	public float timeScale;
	public bool isPM;
	public bool showClock = false;
	public bool showAttributes = false;
	public string clockTime;
	public string attributeDisplay;
	public float dayTimer, morale, energy;
	public float fatigueMultiplier = 1.0f;
	public string dayOfWeek = "Sunday";
	public List<string> tooHigh = new List<string>();
	public List<string> tooLow = new List<string>();
	public List<string> daysOfWeek = new List<string>();
	//{
	//	"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	//};
	public Dictionary<string, float> attributes = new Dictionary<string, float>()
	{
		{"morale", 99f}, {"energy", 99f}, {"entertainment", 50f}, {"money", 49f}, {"satiety", 70f}, {"fitness", 50f}, {"hygiene", 50f}
	};
	
	public float frameTime = 0f;
	public GUIStyle clockStyle;
	private AudioSource watchBeep;
	private EndGame endGame;
	
	void Awake ()
	{
		watchBeep = GetComponent<AudioSource>();
		clockHour = 7;
		clockMinute = 00;
		isPM = false;
		dayCount = 0;
		dayTimer = 17.00f;
		timeScale = 1f / 18.75f;
		daysOfWeek.Add("Sunday");
		daysOfWeek.Add("Monday");
		daysOfWeek.Add("Tuesday");
		daysOfWeek.Add("Wednesday");
		daysOfWeek.Add("Thursday");
		daysOfWeek.Add("Friday");
		daysOfWeek.Add("Saturday");
	}

	void Update () 
	{
		frameTime = 0;
		ProcessEvents();
	}

	void LateUpdate()
	{
		CalculateTime();
		CalculateAttributes();
	}

	void CalculateTime ()
	{
		frameTime += Time.deltaTime * timeScale;
		dayTimer -= frameTime;

		if (dayTimer <= 0)
		{
			dayTimer += 24f;
			isPM = false;
			dayCount++;
			attributes["money"] -= 20f;
			if (dayCount > 6)
			{
				dayCount = 0;
			}
			dayOfWeek = daysOfWeek[dayCount];
		}

		if (isPM)
		{
			clockHour = (int)(12 - dayTimer);
		}
		else if (dayTimer > 12)
		{
			clockHour = (int)(24 - dayTimer);
		}
		else
		{
			isPM = true;
			clockHour = (int)(12 - dayTimer);
		}

		if (clockHour == 0)
		{
			clockHour = 12;
		}

		clockMinute = (int)(60 - (dayTimer - (int)dayTimer) * 60);

		if (clockMinute >= 10)
		{
			clockTime = clockHour + ":" + clockMinute;
		}
		else
		{
			clockTime = clockHour + ":0" + clockMinute;
		}

		if (isPM)
		{
			clockTime += " PM";
		}
		else
		{
			clockTime += " AM";
		}
	}

	void CalculateAttributes()
	{	
		attributes["satiety"] -= frameTime * 4f;
		attributes["hygiene"] -= frameTime * 2f;
		attributes["energy"] -= frameTime * fatigueMultiplier;

		attributes["entertainment"] -= frameTime;
		attributes["morale"] -= frameTime * 1f;
		attributes["morale"] += ((int)(attributes["entertainment"] / 20f)) * frameTime;

		attributes["health"] = attributes["satiety"]/3 + attributes["fitness"]/3 + attributes["hygiene"]/3;
		attributes["morale"] -= frameTime * 3f;
		attributes["morale"] += ((int)(attributes["health"] / 20f)) * frameTime;

		attributes["morale"] -= frameTime * 1f;
		attributes["morale"] += ((int)(attributes["money"] / 25f)) * frameTime;

		if(attributes["energy"] < 20)
		{
			attributes["morale"] -= frameTime * 1f;
		}

		foreach (KeyValuePair<string, float> pair in attributes)
		{
			if (pair.Value < 0)
			{
				tooLow.Add(pair.Key);
			}
			else if (pair.Value >= 100)
			{
				tooHigh.Add(pair.Key);
			}
		}

		foreach (string attribute in tooLow)
		{
			attributes[attribute] = 0f;
		}

		foreach (string attribute in tooHigh)
		{
			attributes[attribute] = 99f;
		}

		tooLow.Clear();
		tooHigh.Clear();

		morale = attributes["morale"];
		energy = attributes["energy"];

		attributeDisplay = "Attributes:";

		foreach (KeyValuePair<string, float> pair in attributes)
		{
			attributeDisplay += "\n" + pair.Key + ":" + (int)pair.Value;
		}
	}

	void ProcessEvents()
	{
		if(Input.GetKeyDown(KeyCode.C) && showClock == false)
		{
			showClock = true;
			watchBeep.Play();
		}
		else if(Input.GetKeyUp(KeyCode.C) && showClock == true)
		{
			showClock = false;
		}

		if(Input.GetKeyDown(KeyCode.X) && showAttributes == false)
		{
			showAttributes = true;
		}
		else if(Input.GetKeyUp (KeyCode.X) && showAttributes == true)
		{
			showAttributes = false;
		}
	}
	
	void OnGUI()
	{
		if (showClock)
			GUI.Label(new Rect(Screen.width/2 - 20, Screen.height/10, Screen.width/5, Screen.height/5), clockTime, clockStyle);

		if (showAttributes)
			GUI.Label(new Rect(Screen.width/7, Screen.height - Screen.height/3, Screen.width/3, Screen.height/3), attributeDisplay);
	}
}
